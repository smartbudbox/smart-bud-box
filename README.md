# Smart-Bud-Box
A small full automatic Growbox with Web Interface for everyone.

I already uploaded the Arduino Sketch for Wemos D1 mini wich connects to the WebServer.

At build_the_box u find the schematics and gerber files for the Board with KiCAD (freeware) and the 3D Model at FreeCAD (also freeware).

The mdf_cutting_example.png is and example how to cut the wodden boards from a big one, i gave it to the warehouse and they cut it for me.

# Alpha Install
- Configure the wemos.ino and flash with Arduino wich u can find at /build_the_box/arduino...
- Build the PCB.
- Upload /web files to your Webhost and run /install/index.php
- Power on and Login into your  Webinterface


## Todo
- complete the "Planer" to make plans like watering if soil under 360
- languages files for multilang
- write a growbook and put it to interface
- put the instrcutions for building and install to project as .pdf
- Addons like: libra, temp/humi for othside

## Wanna help?
You can help me by translate the language files (comming soon) into your langauge or make a new web design =)